angular.module( 'sailng.createdb', [
])

.config(function config( $stateProvider ) {
    $stateProvider.state( 'objectontos', {
        url: '/createdb',
        views: {
            "main": {
                controller: 'createdbController',
                templateUrl: 'createdb/index.tpl.html'
            }
        }
    });
})

.controller( 'createdbController', function ObjectontosController( $scope,  $state, EntityManagerProvider, config, MenuManagerProvider, $templateCache, $http ) {

    $scope.currentUser = config.currentUser;

    var EntityManager = EntityManagerProvider('constructor/subjectarea');

    $scope.subjectAreas = [];
    $scope.subjectArea = {};
    $scope.refreshModel = refreshModel;
    $scope.createdatabase = createdatabase;

    function refreshModel(){
      $scope.subjectAreas = [];
      EntityManager.count().then( function ( items ) {
        if(!items){
          items = { count: 0 }
        }
        var itemsPerPage = 100;
        for(var i = 0; i <= items.count; i = i + itemsPerPage ){
          EntityManager.listPagination( i/itemsPerPage, itemsPerPage ).then( function ( entities ) {
            $scope.subjectAreas = $scope.subjectAreas.concat(entities);
          });
        }
      });
    }
    function createdatabase(){
      if($scope.subjectArea.object){
        EntityManager.createDataBase( $scope.subjectArea.object.id ).then( function ( result ) {
          MenuManagerProvider.loadMenu();
          $templateCache.put('ngForms/getsidebar','hello');
        });
      }else{
        alert('Для создания базы данных нужно выбрать предметную область')
      }
    }

  });

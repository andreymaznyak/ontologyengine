angular.module( 'sailng.home', [
])

.config(function config( $stateProvider ) {
	$stateProvider.state( 'home', {
		url: '/home',
		views: {
			"main": {
				controller: 'HomeCtrl',
				templateUrl: 'home/index.tpl.html'
			}
		}
	});
})

.controller( 'HomeCtrl', function HomeController( $scope, titleService ) {

  titleService.setTitle('Home');
  $scope.myInterval = 3000;
  var slides = $scope.slides = [];
  $scope.addSlide = function( text, image ) {
    var newWidth = 479 + slides.length + 1;
    slides.push({
      image: image,
      text: text
    });
  };
  var image = '/images/sliderimg1.jpg';
  $scope.addSlide('Авторизуйтесь', image);
  $scope.addSlide('Добавьте предметные области', image);
  $scope.addSlide('Добавьте онтологии', image);
  $scope.addSlide('Добавьте объекты онтологий', image);
  $scope.addSlide('Создавайте базу данных', image);
  $scope.addSlide('Редактируйте сущности созданной онтологии', image);
});

angular.module( 'ontologyEngine.dashboard', [
])

.config(function config( $stateProvider ) {
    $stateProvider.state( 'dashboard', {
        url: '/dashboard',
        views: {
            "main": {
                controller: 'dashboardController',
                templateUrl: 'dashboard/index.tpl.html'
            }
        }
    });
})

.controller( 'dashboardController', function dashboardController( $scope,  $state, localStorageService, MetadataManager ) {


    var name = $state.current.name;

    var model = localStorageService.get(name);
    if (!model) {
      // set default model for demo purposes
      model = {
        structure: "main",
        title: name,
        rows: [{
          columns: [{
            styleClass: "col-md-2",
            widgets: []
          }, {
            styleClass: "col-md-10",
            widgets: []
          }]
        }]
      };
      var action = $state.current.name.split('.')[1], entityname = $state.current.name.split('.')[0];
      MetadataManager.getWidgets( action, entityname ).then(function(data){
        $scope.model = data;
      });
    }
    $scope.name = name;
    $scope.model = model;
    $scope.collapsible = true;
    $scope.maximizable = true;

    $scope.$on('adfDashboardChanged', function (event, name, model) {
      localStorageService.set(name, model);
    });
    /*$scope.currentUser = config.currentUser;

    var EntityManager = EntityManagerProvider('constructor/subjectarea');

    $scope.subjectAreas = [];
    $scope.subjectArea = {};
    $scope.refreshModel = refreshModel;
    $scope.createdatabase = createdatabase;

    function refreshModel(){
      $scope.subjectAreas = [];
      EntityManager.count().then( function ( items ) {
        if(!items){
          items = { count: 0 }
        }
        var itemsPerPage = 100;
        for(var i = 0; i <= items.count; i = i + itemsPerPage ){
          EntityManager.listPagination( i/itemsPerPage, itemsPerPage ).then( function ( entities ) {
            $scope.subjectAreas = $scope.subjectAreas.concat(entities);
          });
        }
      });
    }
    function createdatabase(){
      if($scope.subjectArea.object){
        EntityManager.createDataBase( $scope.subjectArea.object.id ).then( function ( result ) {
          MenuManagerProvider.loadMenu();
          $templateCache.put('ngForms/getsidebar','hello');
        });
      }else{
        alert('Для создания базы данных нужно выбрать предметную область')
      }
    }
*/
  });

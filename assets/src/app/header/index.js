angular.module( 'sailng.header', [
  'lodash',
  'ui.bootstrap'
])
.controller( 'NavbarCtrl', function HeaderController( $scope, $state, $stateParams, config, MenuManagerProvider ) {

    $scope.isCollapsed = true;

    if(config.currentUser==false && typeof (currentUser) != 'undefined'){
      config.currentUser = currentUser;
    }
    $scope.currentUser = config.currentUser;

    var navItems = [];



    navItems.push({title: 'About', translationKey: 'navigation:about', url:'/about',cssClass: 'fa fa-info-circle'});
    navItems.push({title: 'Dashboard', translationKey: 'navigation:dashboard', url:'/dashboard',cssClass: 'fa fa-info-circle'});


    if(!$scope.currentUser) {
        navItems.push({title: 'Login', translationKey: 'navigation:login', url: '/login', cssClass: 'fa fa-sign-in'});
    }else{
      navItems.push({title: 'Create database', translationKey: 'navigation:createdb', url:'/createdb',cssClass: 'fa fa-database'});
      //navItems.push({title: 'Register', translationKey: 'navigation:register', url: '/register', cssClass: 'fa fa-briefcase'});
    }
    MenuManagerProvider.getMenu().then(function(results){
      $scope.navItemsFolder = [].concat(results);
    });

    $scope.navItems = navItems;
})
;





angular.module( 'sailng', [
    'ui.router',
    'sails.io',
    'angularMoment',
    'lodash',
    'ui.bootstrap',
    'templates-app',
    'services',
    'pascalprecht.translate',
    'models',
    'directives',
    'sailng.createdb',
    'sailng.header',
    'sailng.home',
    'sailng.about',
    'ontologyEngine.dashboard',
    'oc.lazyLoad',
    'sailng.messages',
    'ui.select',
    'LocalStorageModule',
    'adf',
    'adf.structures.base',
    'widgets',
    //'adf.structures.base',
    'ngSanitize'
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider, $locationProvider, $translateProvider, $ocLazyLoadProvider ) {
    //$urlRouterProvider.otherwise( '/' );
    window["metadata_loading"] = true;
    $urlRouterProvider.otherwise(function ($injector, $location, $state ) {
        var location_url = $location.$$url;
        setTimeout(check_loading, 2500);
        function check_loading($state){
          if(!window["metadata_loading"]){
            $state.go(location_url);
          }else{
            setTimeout(check_loading, 500);
          }
        }
      /*
        if ($location.$$url === '/') {
            window.location = '/home';
        }
        else if($location.$$url === '/login' || $location.$$url === '/register' || $location.$$url === '/logout'){
            // pass through to let the web server handle this request
            window.location = $location.$$absUrl;
        }else{
          window.location = '/home';
        }
       */
    });

    $locationProvider.html5Mode(true);

    $ocLazyLoadProvider.config({
      debug: true
    });
   /* $translateProvider.useStaticFilesLoader({
      prefix: '/locales/',
      suffix: '.json'
    }).preferredLanguage('ru');
*/
})


.run( function run (MenuManagerProvider) {
    console.log('START');
    MenuManagerProvider.loadMenu();
    moment.locale('ru');
})

.controller( 'AppCtrl', function AppCtrl ( $scope, config ) {
    config.currentUser = window.currentUser;
});

angular.module('adf.widget.sidebar', ['adf.provider'])
  .config(function(dashboardProvider){
    dashboardProvider
      .widget('Sidebar', {
        title: 'Sidebar',
        reload: true,
        description: 'Displays a navigation sidebar',
        controller: 'SidebarCtrl',
        templateUrl: 'ngForms/getSidebar'
      });
  }).controller( 'SidebarCtrl', function HeaderController( $scope, $state, $stateParams, config, $templateCache, $http ) {

    $('label.tree-toggler').click(function () {
      $(this).parent().children('ul.tree').toggle(300);
    });

    //$scope.sidebartemplate = 'ngForms/getsidebar';
    //
    //$http.get($scope.sidebartemplate).then(function(data){
    //
    //  $templateCache.put($scope.sidebartemplate,data.data);
    //
    //  console.log('refresh complite');
    //})
    //
    //reloadMenu(true);
    //var forceload = true;
    //function reloadMenu(){
    //  var sidebarurl = 'ngForms/getsidebar';
    //
    //  console.log($templateCache.get(sidebarurl));
    //
    //  if(!$templateCache.get(sidebarurl)){
    //    $http.get(sidebarurl).then(function(data){
    //
    //      $templateCache.put('ngForms/getsidebar',data.data);
    //
    //    })
    //    sidebarurl = 'ngForms/getnavbar';
    //    console.log('refresh complite');
    //  }
    //
    //  $scope.sidebartemplate = sidebarurl;
    //  //$http.get(sidebarurl).then(function(data){
    //  //$templateCache.remove(sidebarurl);
    //  //$templateCache.put(sidebarurl,data.data);
    //
    //  // $scope.sidebartemplate = sidebarurl;
    //  //console.log('refresh complite');
    //  //})
    //  //setTimeout(reloadMenu, 5000);
    //}
  });

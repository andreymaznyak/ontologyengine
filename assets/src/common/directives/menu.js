angular.module('directives.menu', [])
  .directive('menu', function() {
  return {
    restrict: 'A',
    scope: {
      menu: '=menu',
      cls: '=ngClass'
    },
    replace: true,
    template: '<ul class="dropdown-menu"><li ng-repeat="item in menu" menu-item="item"><i class="{{item.cssClass}}"></i></li></ul>',
    link: function(scope, element, attrs) {
      if (element.ngif) {
        element.addAttribute('ng-if',element.ngif);
      }
      element.addClass(attrs.class);
      element.addClass(scope.cls);
    }
  };
})
  .directive('menuItem', function($compile) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        item: '=menuItem'
      },
      template: '<li active-link><a href={{item.href}}><i class="{{item.cssClass}}"></i>{{item.title | translate }}</a></li>',
      link: function (scope, element, attrs) {
        if (scope.item.header) {
          element.addClass('nav-header');
          element.append('<i class="fa fa-yelp"></i>');
          element.text(scope.item.header);
        }
        if (scope.item.ngif) {
          element.addAttribute('ng-if',scope.item.ngif);
        }
        if (scope.item.divider) {
          element.addClass('divider');
          element.empty();
        }
        if (scope.item.submenu) {
          element.addClass('dropdown');

          var text = element.children('a').text();
          element.empty();
          var $a = $('<a class="dropdown-toggle">'+text+'</a>');
          element.append($a);

          var $submenu = $('<div menu="item.submenu" class="dropdown-menu"></div>');
          element.append($submenu);
        }
        if (scope.item.click) {
          element.find('a').attr('ng-click', 'item.click()');
        }
        $compile(element.contents())(scope);
      }
    };
  })

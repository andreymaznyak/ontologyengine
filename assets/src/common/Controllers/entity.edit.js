/**
 * Created by Андрей on 14.06.2015.
 */
angular
  .module( 'controller.entity.edit', [] )
  .controller( 'editController', [ '$scope', '$state', '$stateParams', 'utils', 'entityRepository', 'EntityManagerProvider', editController ] );

function editController( $scope, $state, $stateParams, utils, entityRepository, EntityManagerProvider ) {
  var entityId = $stateParams.id;
  console.log(entityId);
  if( typeof(entityId) != 'undefined' ){
    entityRepository.getOne( entityId ).then(function( data ){
      $scope.entity = data;
    });
  }

  $scope.goBack = goBack;
  $scope.submit = submit;

  $scope.destroySubentity = destroySubentity;
  $scope.addSubEntity = addSubEntity;

  $scope.refreshModel = refreshModel;

  $scope.assotiationsEntity = { main: {} };
  $scope.getAssotiationsEntity = getAssotiationsEntity;

  function getAssotiationsEntity(alias, subentity){
    if(!subentity) var subentity = 'main';

    if(!$scope.assotiationsEntity[subentity])
      $scope.assotiationsEntity[subentity] = {}

    if(!$scope.assotiationsEntity[subentity][alias])
      $scope.assotiationsEntity[subentity][alias] = [];

    return $scope.assotiationsEntity[subentity][alias];
  }

  function refreshModel(alias, subentity){
    if(!subentity) var subentity = 'main';

    if(!$scope.assotiationsEntity[subentity]) $scope.assotiationsEntity[subentity] = {}

    $scope.assotiationsEntity[subentity][alias] = [];
    var EntityManager = EntityManagerProvider(alias);
    EntityManager.count().then( function ( items ) {
      if(!items){
        items = { count: 0 }
      }
      var itemsPerPage = 100;
      for(var i = 0; i <= items.count; i = i + itemsPerPage ){
        EntityManager.listPagination( i/itemsPerPage, itemsPerPage ).then( function ( entities ) {
          $scope.assotiationsEntity[subentity][alias] = $scope.assotiationsEntity[subentity][alias].concat(entities);
        });
      }
    });
  }

  function addSubEntity(subentity){
    $scope.entity[subentity].push({});
  }

  function destroySubentity(subentity, id){
    $scope.entity[subentity].splice(id,1);
  }
  /**
   * Go list view
   */
  function goBack() {
    $state.go( '^.list', $stateParams );
  }

  /**
   * Submit form
   */
  function submit() {

    return entityRepository
      .update( $scope.entity )
      .then( entityCreated, entityNotCreated );

    function entityCreated( data )
    {
      $state.go( '^.list', $stateParams );
    }

    function entityNotCreated( response )
    {
      if ( response.exception ) {

        console.log( response.exception );
      } else {
        //Messenger().post({
        //    type: "error",
        //    message: 'Упс... Ошибочки при заполнении полей...',
        //    showCloseButton: true
        //});
        //
        //utils.processErrors( $scope.formName + 'Form', response );
      }
    }
  }
}

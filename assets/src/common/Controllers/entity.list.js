angular
  .module( 'controller.entity.list', [
    //'ngTable'
  ] )
  .controller( 'listController', [ '$scope', '$state', '$stateParams', 'utils', 'entityRepository', listController ] );

function listController( $scope, $state, $stateParams, utils, entityRepository ) {

  $scope.newEntity = function(){
    $state.go( '^.new', $stateParams );
  }
  $scope.editEntity = function(id){
    $stateParams.id = id;
    $state.go( '^.edit', $stateParams );
  }
  var entities = $scope.pageEntities = entityRepository.entities;

  /*
  $scope.destroy = destroy;
  $scope.listEntities = listEntities;
  $scope.pageChanged = onPageChanged;
  $scope.Page = 1;

  initialization();
  listEntities( $scope.Page, $scope.itemsPerPage );
  onPageChanged( $scope );

  function destroy(id) {
    entityRepository.destroy(id).then( function(){
      var pages = $scope.entities;
      for( var i in pages ) {
        if ( pages[i].Page == $scope.Page ) {
          var entityInPage = pages[i].list;
          for(var j in entityInPage){
            if(entityInPage[j].id == id){
              entityInPage.splice(j,1);
              break;
            }
          }
          break;
        };
      }
      initialization();
      onPageChanged();
    })
  }

  function initialization(){
    $scope.loadPages = [];
    $scope.entities = [{page : 0,  list : []}];
    $scope.itemsPerPage = 10;
    $scope.pageEntities = [];
  }
  /**
   * Reload entities on Page
   *//*
  function onPageChanged() {
    console.log( $scope.Page, $scope.loadPages, $scope.entities );
    if($scope.loadPages.indexOf( $scope.Page ) < 0){
      listEntities( $scope.Page, $scope.itemsPerPage );
    };
    for( var i in $scope.entities ) {
      if ( $scope.entities[i].Page == $scope.Page ) {
        $scope.pageEntities = $scope.entities[i].list;
      };
    }
  };

  /**
   * Find all existing entities on page
   *//*
  function listEntities( page, itemsPerPage ) {

    entityRepository.count().then( function ( items ) {
      if(!items){
        items = { count: 0 }
      }
      $scope.totalItems = items.count;
    });

    entityRepository.listPagination( page, itemsPerPage ).then( function ( entities ) {
      var result = true;
      for( var i in $scope.entities ) {
        if ( $scope.entities[i].Page == page ) {
          result = false;
        };
      }

      if( !!entities && entities.length > 0 && !!result ){
        var item = {
          Page : page,
          list : entities
        };
        $scope.entities.push( item );
        $scope.pageEntities = entities;

        $scope.loadPages.push( page );
        $scope.loadPages.sort();
      }
    });

  }
*/
}

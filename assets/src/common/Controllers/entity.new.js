angular
    .module( 'controller.entity.new', [] )
    .controller( 'newController', [ '$scope', '$state', '$stateParams', 'utils', 'entityRepository','EntityManagerProvider', newController ] );

function newController( $scope, $state, $stateParams, utils, entityRepository, EntityManagerProvider ) {

    $scope.goBack = goBack;
    $scope.submit = submit;
    $scope.entity = {};

    $scope.destroySubentity = destroySubentity;
    $scope.addSubEntity = addSubEntity;

    $scope.refreshModel = refreshModel;
    $scope.assotiationsEntity = { main: {} };
    $scope.getAssotiationsEntity = getAssotiationsEntity;

    function getAssotiationsEntity(alias, subentity){
      if(!subentity) var subentity = 'main';

      if(!$scope.assotiationsEntity[subentity])
        $scope.assotiationsEntity[subentity] = {}

      if(!$scope.assotiationsEntity[subentity][alias])
        $scope.assotiationsEntity[subentity][alias] = [];

      return $scope.assotiationsEntity[subentity][alias];
    }

    function refreshModel(alias, subentity){
      if(!subentity) var subentity = 'main';

      if(!$scope.assotiationsEntity[subentity]) $scope.assotiationsEntity[subentity] = {}

      $scope.assotiationsEntity[subentity][alias] = [];
      var EntityManager = EntityManagerProvider(alias);
      EntityManager.count().then( function ( items ) {
        if(!items){
          items = { count: 0 }
        }
        var itemsPerPage = 100;
        for(var i = 0; i <= items.count; i = i + itemsPerPage ){
          EntityManager.listPagination( i/itemsPerPage, itemsPerPage ).then( function ( entities ) {
            $scope.assotiationsEntity[subentity][alias] = $scope.assotiationsEntity[subentity][alias].concat(entities);
          });
        }
      });
    }

    function addSubEntity(subentity){
      if(!$scope.entity[subentity]) $scope.entity[subentity] = []
      $scope.entity[subentity].push({});
    }

    function destroySubentity(subentity, id){
      $scope.entity[subentity].splice(id,1);
    }
    /**
     * Go list view
     */
    function goBack() {
        $state.go( '^.list', $stateParams );
    }


}

/**
 * Created by Андрей on 21.06.2015.
 */
var navItemsFolder = [];
angular
  .module('services.menu-manager', ['lodash', 'services', 'sails.io'])
  .service('MenuManagerProvider', ['EntityManagerProvider', 'ModuleManagerProvider', '$ocLazyLoad', 'lodash','$q','utils', 'config', MenuManager] );

function MenuManager(EntityManagerProvider, ModuleManagerProvider, $ocLazyLoad, lodash, $q, utils){
  return {
    loadMenu: function(callback){
      var EntityManager = EntityManagerProvider('home');
      EntityManager.getAllFolder().then(function(folders){
        for(var i in folders){
          //RoutingManagerProvider.CreateFolderRouting(folders[i]);
          EntityManager.getAllEntityInFolder(folders[i]).then(function(result){
            var entities = result.entities;
            var navItemsChild = [];
            for(var j in entities){
              var entityModule = ModuleManagerProvider.CreateEntityRouting(result.folder,entities[j]);
              $ocLazyLoad.inject(entityModule.module_name);
              navItemsChild.push({title:entities[j], translationKey:'navigation:'+entities[j], href: utils.translit(entityModule.URL.replace('\\','/'),1), cssClass:'fa fa-cog fa-spin'});
            }
            var subTree = parseUrlAndAddToTree( result.folder, navItemsChild, navItemsFolder );

            if( subTree )
              navItemsFolder = navItemsFolder.concat(subTree);
          });
        }
        setTimeout(set_loading_false,1);
        function set_loading_false(){
          window["metadata_loading"] = false;
        }
      });
    },
    getMenu: function(){
      var deferred = $q.defer();

      setTimeout(function() {
          deferred.resolve(navItemsFolder);
      }, 1000);

      return deferred.promise;
    }
  }

  function parseUrlAndAddToTree(uri, lastChild, navItemsFolder){
    var indexComma = uri.indexOf('\\'),
      lastComma = uri.lastIndexOf('\\'),
      currentName = 'error',
      resultSubTree = [],
      currentList = [],
      newSubTree = [],
      navItemsChild;
    if( indexComma > 0 ) {
      currentName = utils.translit(uri.substr(0,indexComma),1);
      var nextUri = utils.translit(uri.replace(currentName + '\\',''),1);
      navItemsChild = [];
      if( indexComma == lastComma ){
        newSubTree = parseUrlAndAddToTree( nextUri, lastChild, navItemsFolder );

        if( newSubTree )
          resultSubTree = resultSubTree.concat( newSubTree );

        currentList = [ { "header": currentName, cssClass:'fa fa-yelp' } ];
      }else{
        newSubTree = parseUrlAndAddToTree( nextUri, lastChild, navItemsFolder );
        if( newSubTree )
          navItemsChild = navItemsChild.concat( newSubTree );
        currentList = [{title:currentName, translationKey:'navigation:'+currentName, cssClass:'fa fa-cog', "submenu": navItemsChild }];

      }
      resultSubTree = currentList.concat(resultSubTree);
    }
    else {
      currentName = utils.translit(uri,1);
      navItemsChild = lastChild;
      resultSubTree.push({title:currentName, translationKey:'navigation:'+currentName,/* cssClass:'fa fa-cog', */"submenu": navItemsChild });
      if( currentName == 'Constructor' ){
        resultSubTree[0]['ngif'] = 'CurrentUser';
      }
    }
    var result = _.findWhere(navItemsFolder,{ title:currentName })
    if( !result ){
      result =  _.findWhere(navItemsFolder,{ header:currentName })
      if( !result )
        return resultSubTree
      else
        return newSubTree
    }
    else
      result.submenu = result.submenu.concat( navItemsChild );
  }

};


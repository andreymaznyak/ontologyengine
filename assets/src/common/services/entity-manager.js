angular
  .module('services.entity-manager', ['lodash', 'services', 'sails.io'])
  .service('EntityManagerProvider', ['lodash', 'utils', '$sailsSocket', 'MetadataManager', '$q', EntityManager] );

function EntityManager(lodash, utils, $sailsSocket, MetadataManager, $q) {
  return function(alias) {

    var entities = this.entities = [];
    var watching = false;
    var itemsPerPage = this.itemsPerPage = 30;
    var lazyLoading = this.lazyLoading = false;

    var routing = {
      list: alias + '/getAll/',
      listPagination: alias + '/getPage/',
      create :  alias + '/create',
      count : alias + '/count/',
      update : function( id ) {
        return alias + '/update/' + id;
      },
      destroy : function( id ) {
        return alias + '/destroy/' + id;
      },
      getOne : function( id ) {
        return alias + '/getOne/' + id;
      }
    }

    var init = this.init = function init( options, callback ){
      $sailsSocket.get(utils.prepareUrl( routing.count )).then(function(response){
        watching = true;

        var count = response.data.count,
          pages = count/itemsPerPage,
          i = 0;

        console.log(pages);
        for(i; i < (pages); i++){

          console.log(apiUrl+'?limit='+itemsPerPage+'&skip=' + (i*itemsPerPage));
          if(lazyLoading && i < (pages - 1)){
            //entities.push([]);
          }else{
            $sailsSocket.get(utils.prepareUrl(alias+'/find?limit='+itemsPerPage+'&skip=' + (i*itemsPerPage))).then(function(response){
              for(var j = 0; j < response.data.length; j++)
                entities.push(response.data[j]);

            },error);
          }
        }
        var match = /(\w+)$/g,
            result = alias.match(match);

        console.log(result);

        $sailsSocket.subscribe(result, function (envelope) {
          switch(envelope.verb) {
            case 'created':
              created(envelope);
              break;
            case 'destroyed':
              deleted({data:{id:envelope.id}});
              break;
          }
        });
        if(!!callback){
          callback();
        }
      },error);
    }
    init();
    return {
      list: list,
      listPagination: listPagination,
      create: create,
      update: update,
      destroy: destroy,
      getOne: getOne,
      count: count,
      createDataBase: MetadataManager.createDataBase,
      getAllFolder: MetadataManager.getAllFolder,
      getAllEntityInFolder: MetadataManager.getAllEntityInFolder
    }
    function list() {
      var url = utils.prepareUrl(routing.list);
      if(!watching){
        init();
      }
      return entities;//.then(success, error);
    };
    function listPagination( page, itemcount ) {
      var url = utils.prepareUrl(routing.list);
      if(!watching){
        init();
      }
      return entities;
      //var url = utils.prepareUrl( routing.listPagination + '?page=' + page + '&itemcount=' + itemcount );
      //return $sailsSocket.get( url ).then( success, error );
    };
    function count(){
      var url = utils.prepareUrl( routing.count );
      return $sailsSocket.get( url ).then( success, error );
    };
    function getOne(id){
      var url = utils.prepareUrl( routing.getOne(id) );
      return $sailsSocket.get( url ).then( success, error );
    };
    function create(newModel) {
      var url = utils.prepareUrl(routing.create);
      var deferred = $q.defer();

      $sailsSocket.post(url, newModel).then(function created_succes(reponse){
        entities.push(reponse.data);
        deferred.resolve(reponse);
      }, function created_error(resp){
        deferred.reject(reponse);
      });

      return deferred.promise;
    };
    function update(Model) {
      var url = utils.prepareUrl(routing.update(Model.id));
      return $sailsSocket.put(url, Model).then(success, error);
    };
    function destroy(id) {
      var url = utils.prepareUrl(routing.destroy(id));
      return $sailsSocket.delete(url).then(success, error);
    };
    function success(response) {
      return response.data;
    };

    function error(error) {
      console.log(error);
    };
  }
}


angular.module( 'services.config', ['lodash'])

.service('config', function(lodash) {

	// private vars here if needed

	return {
		siteName: 'OntologyEngine',
		// no trailing slash!
		siteUrl: '/',
		apiUrl: '/api',
    loading: true,
		currentUser: false
	};

});

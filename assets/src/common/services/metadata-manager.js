angular
  .module('services.metadata-manager', ['lodash', 'services', 'sails.io'])
  .service('MetadataManager', ['lodash', 'utils', '$sailsSocket', MetadataManager] );

function MetadataManager(lodash, utils, $sailsSocket) {

    var routing = {
      getWidgets: function (action, entityName){
        return 'ngForms/getWidgets/?action=' + action + '&entityname=' + entityName;
      },
      createdatabase: function(subjectarea){
        return 'database/create/' + subjectarea;
      },
      folderlist: 'entitymanager/getAllFolders',
      entityinfolder: function( folder ){
        return 'entitymanager/getAllEntityInFolder/?folder=' + folder;
      }
    }

    return {
      createDataBase: createDataBase,
      getAllFolder: getAllFolder,
      getAllEntityInFolder: getAllEntityInFolder,
      getWidgets: getWidgets
    }
    function createDataBase( subjectareaid ){
      var url = utils.prepareUrl(routing.createdatabase(subjectareaid));
      return $sailsSocket.get(url).then( success, error );
    };

    function getAllFolder(){
      var url = utils.prepareUrl(routing.folderlist);
      return $sailsSocket.get(url).then( success, error );
    };

    function getAllEntityInFolder(nameFolder) {
      var url = utils.prepareUrl( routing.entityinfolder( nameFolder ) );
      return $sailsSocket.get(url).then( success, error );
    };

    function getWidgets( action, entityName ){
      var url = '/' + routing.getWidgets( action, entityName );
      return $sailsSocket.get(url).then( success, error )
    };

    function success(response) {
      return response.data;
    };

    function error(error) {
      console.log(error);
    };

}


'use strict';

angular
  .module('services.modules-factory', ['lodash', 'services', 'sails.io'])
  .service('ModuleManagerProvider', ['lodash', 'utils', '$sailsSocket', ModuleManager] );

function ModuleManager( lodash, utils ) {
  return { CreateEntityRouting: CreateEntityModule }

  function CreateEntityModule( folderName, entityName, widgets ){


    var folder_name = folderName.replace(/\\/g,'.'),
        entity_name = entityName.replace(/\//g,'.'),
        module_name = 'ontology.' + folder_name + '.' + entity_name,
        service_name = entity_name + 'Provider';

    if(!widgets)
      widgets = getDefaultWidgetsForEntity(entity_name, service_name);

    /* configure entity module */
    var entity_module =
      angular.module(module_name, [
                                'ui.router',
                                'adf.provider'
                              ])
    .config(['$stateProvider', 'dashboardProvider', configurationLocal])
    .service(service_name, ['lodash', 'utils', '$sailsSocket', '$q', serviceConstructor]);

    /* generate controllers*/
    for(var i = 0; i < widgets.length; i++){
      entity_module.controller(widgets[i].controller.name,widgets[i].controller.source);
    }

    return { module_name: module_name, URL: getURL( folder_name, entity_name ) };

    /* register adf module - widget */
    function each_register_widgets(module_name,widget){

      var widget_name = widget.name + entityName,
        template_url = widget.template + '&object=' + entityName;

      angular.module( module_name, ['adf.provider'] )
        .config(function( dashboardProvider ){
          dashboardProvider
            .widget( widget_name + Math.random(), {
              title: widget_name,
              description: widget_name,
              controller:  widget.controller.name,
              templateUrl: template_url

            });
        });
    }

    /* return url, created by folder name and entity name */
    function getURL( folder_name, entityName ){

      return '/' + folder_name.replace(/\\/g,'/').replace(/\./g,'/') + '/' + entityName;

    }

    /* generate entity states and widgets */
    function configurationLocal( $stateProvider, dashboardProvider ) {

      var states = getDefaultStatesForEntity(entityName);

      /* define widgets */
      for(var i = 0; i< widgets.length; i++){
        var widget_name = widgets[i].name + entityName;
        dashboardProvider
          .widget( widget_name, {
            title: widget_name,
            description: widget_name,
            controller:  widgets[i].controller.name,
            templateUrl: widgets[i].template + '&object=' + entityName
          });
      }

      /* define states */
      for( var i = 0; i < states.length; i++ ){
        $stateProvider.state( states[i].name, states[i].source );
      }

    }

    function serviceConstructor(lodash, utils, $sailsSocket, $q){

      var entities = this.entities = [],
          watching = false,
          itemsPerPage = this.itemsPerPage = 30,
          lazyLoading = this.lazyLoading = false,
          prefix = folderName.replace(/\\/g,'/') + '/' + entityName;

      var routing = {
        list: prefix + '/getAll/',
        listPagination: prefix + '/getPage/',
        create : prefix + '/create',
        count : prefix + '/count/',
        update : function( id ) {
          return prefix + '/update/' + id;
        },
        destroy : function( id ) {
          return prefix + '/destroy/' + id;
        },
        getOne : function( id ) {
          return prefix + '/getOne/' + id;
        }
      }

      var init = this.init = function init( callback ){
        $sailsSocket.get(utils.prepareUrl( routing.count )).then(function(response){
          watching = true;

          var count = response.data.count,
            pages = count/itemsPerPage,
            i = 0;

          console.log(pages);
          for(i; i < (pages); i++){

            console.log(prefix+'?limit='+itemsPerPage+'&skip=' + (i*itemsPerPage));
            if(lazyLoading && i < (pages - 1)){
              //entities.push([]);
            }else{
              $sailsSocket.get(utils.prepareUrl(prefix +'/find?limit='+itemsPerPage+'&skip=' + (i*itemsPerPage))).then(function(response){
                for(var j = 0; j < response.data.length; j++)
                  entities.push(response.data[j]);

              },error);
            }
          }
          var match = /(\w+)$/g,
            result = entity_name.match(match);

          console.log(result);

          $sailsSocket.subscribe(result, function (envelope) {
            switch(envelope.verb) {
              case 'created':
                created(envelope);
                break;
              case 'destroyed':
                deleted({data:{id:envelope.id}});
                break;
            }
          });
          if(!!callback){
            callback();
          }
        },error);
      }

      init();

      this.list = function list() {
        var url = utils.prepareUrl(routing.list);
        if(!watching){
          init();
        }
        return entities;//.then(success, error);
      };

      this.listPagination = function listPagination( page, itemcount ) {
        var url = utils.prepareUrl(routing.list);
        if(!watching){
          init();
        }
        return entities;
        //var url = utils.prepareUrl( routing.listPagination + '?page=' + page + '&itemcount=' + itemcount );
        //return $sailsSocket.get( url ).then( success, error );
      };

      this.count = function count(){
        var url = utils.prepareUrl( routing.count );
        return $sailsSocket.get( url ).then( success, error );
      };

      this.getOne = function getOne(id){
        var url = utils.prepareUrl( routing.getOne(id) );
        return $sailsSocket.get( url ).then( success, error );
      };

      this.createEntity = function create(newModel) {
        var url = utils.prepareUrl(routing.create);
        var deferred = $q.defer();

        $sailsSocket.post(url, newModel).then(function created_success(reponse){
          if(!reponse.data.updated)
            entities.push(reponse.data);
          else{
            var result = _.findWhere(entities, { 'name': reponse.data.name});
            !!result ? result.count++ : result;
          }

          deferred.resolve(reponse);
        }, function created_error(reponse){
          deferred.reject(reponse);
        });

        return deferred.promise;
      };

      this.update = function update(Model) {
        var url = utils.prepareUrl(routing.update(Model.id));
        return $sailsSocket.put(url, Model).then(success, error);
      };

      this.destroy = function destroy(id) {
        var url = utils.prepareUrl(routing.destroy(id));
        var deferred = $q.defer();

        $sailsSocket.delete(url).then(function destroy_success(response){
          var i = _.remove(entities, {id: response.data.id});
          deferred.resolve(response);
        }, function destroy_error(response){
          deferred.reject(response);
        });

        return deferred.promise;
      };

      function success(response) {
        return response.data;
      };

      function error(error) {
        console.log(error);
      };
    }

    /* return default widgets for entity : list, new, edit */
    function getDefaultWidgetsForEntity( entityName, service_name ){
      var default_widgets =
        [
          {
            name: 'list',
            controller:{name:'listController' + entityName, source: [ '$scope', '$state', service_name, listCtrl ]},
            template:'/ngforms?formname=default&formtype=index'
          },
          {
            name: 'new',
            controller:{name:'newController' + entityName, source: [ '$scope', '$state', service_name, newCtrl ]},
            template:'/ngforms?formname=default&formtype=new'
          },
          {
            name: 'edit',
            controller:{name:'editController' + entityName, source: [ '$scope', '$state', service_name, editCtrl ]},
            template:'/ngforms?formname=default&formtype=edit'
          }
        ];

      return default_widgets;

      /*
      *  start define default controllers
      */

      /* default list controller for entity */
      function listCtrl($scope, $state, entityRepository){

        $scope.newEntity = function(){
          $state.go( '^.new', $stateParams );
        }
        $scope.editEntity = function(id){
          $stateParams.id = id;
          $state.go( '^.edit', $stateParams );
        }

        $scope.destroy = function(id){
          entityRepository.destroy(id).then( function(){

          })
        }

        var entities = $scope.pageEntities = entityRepository.entities;


      }
      /* default new controller for entity */
      function newCtrl($scope, $state, entityRepository){

        $scope.createEntity = createEntity;
        $scope.entity = {};
        /**
         * Submit form
         */
        function createEntity() {

          return entityRepository
            .createEntity( $scope.entity )
            .then( entityCreated, entityNotCreated );

          function entityCreated( data )
          {
            $scope.entity = {};
          }

          function entityNotCreated( response )
          {
            if ( response.exception ) {

              console.log( response.exception );
            } else {
              //Messenger().post({
              //    type: "error",
              //    message: 'Упс... Ошибочки при заполнении полей...',
              //    showCloseButton: true
              //});
              //
              //utils.processErrors( $scope.formName + 'Form', response );
            }
          }
        }
      }
      /* default edit controller for entity */
      function editCtrl(){

      }

      /*
      * end default controllers
      */

    }

    /* return default states for entity: entity -> entity.new , entity.list , entity.edit */
    function getDefaultStatesForEntity( entityName ){
      var default_states =
        [
          {
            name:entityName,
            source:{
              abstract: true,
              views: {
                "main": {
                  template: '<ui-view/>',
                }
              },
              url: getURL(folder_name, entity_name),
            }
          },
          {
            name:entityName + '.new',
            source: {
              url: '/new',
              controller: 'dashboardController',
              template: '<adf-dashboard name="{{name}}" collapsible="{{collapsible}}" maximizable="{{maximizable}}" structure="main" adf-model="model" />'
            }
          },
          {
            name:entityName + '.list',
            source: {
              url: '',
              controller: 'dashboardController',
              template: '<adf-dashboard name="{{name}}" collapsible="{{collapsible}}" maximizable="{{maximizable}}" structure="main" adf-model="model" />'
              }
          },
          {
            name:entityName + '.edit',
            source: {
              url: '/{id:int}/edit',
              controller: 'dashboardController',
              template: '<adf-dashboard name="{{name}}" collapsible="{{collapsible}}" maximizable="{{maximizable}}" structure="main" adf-model="model" />'
            }
          }
        ];
      return default_states;
    }
  }
}

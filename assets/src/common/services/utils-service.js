(function( angular ) {
    'use strict';

    angular
        .module( 'service.utils.service', [ ] )
        .factory( 'utils', [ '$timeout', '$parse', factoryProduction ] );

    function factoryProduction( $timeout, $parse ) {
        return {
            // mutex for fix multiple actions/submit.
            mutexAction: function mutexAction(scope, semaphoreName, action) {
                return function() {
                    if (scope[semaphoreName]) {
                        console.log( 'Multi-action prevented for: ' + semaphoreName + ' fired by ', event );
                        // not queuing action, forget it!
                        return;
                    }
                    scope[semaphoreName] = true;

                    action.apply( this||window, arguments ).finally(function () {
                        scope[semaphoreName] = false;
                    });
                };
            },
            // Util for finding an object by its 'id' property among an array
            findById: function findById( a, id ) {
                for ( var i = 0; i < a.length; i++ ) {
                    if ( a[i].id == id ) {
                        return a[i];
                    }
                }
                return null;
            },

            // form-entity to json-entity
            toJSON: function toJSON( entity ) {
                if ( entity == null || typeof( entity ) != 'object' )
                    return entity;

                ///** if choice type */
                //if ( undefined !== entity.selected ) {
                //    return entity.selected ? entity.selected.value : null;
                //}

                var temp = new entity.constructor();

                for ( var key in entity )
                    temp[key] = toJSON( entity[key] );

                return temp;
            },

            // model[field][name] to model.field.name
            propertyToModel: function propertyPathToModel( property ) {
                return property
                    .replace( /\[/g, "." )
                    .replace( /\]/g, "" );
            },

            // process fields errors
            processErrors: function processErrors( formName, errors ) {
                angular.forEach( errors, function ( error, i ) {
                    var $field = $( "[name='" + error.property + "']", "[name='" + formName + "']"),
                        field = $field.parsley();

                    if ( field.reset ) {
                        field.reset();

                        window.ParsleyUI.addError( field, "asyncError", error.message );

                        $field.off( 'focus click', removeError );
                        $field.on( 'focus click', removeError );
                    } else {
                        Messenger().post({
                            type: "error",
                            message: '"' + error.property + '" : ' + error.message,
                            showCloseButton: true
                        });
                    }

                    function removeError() {
                        $timeout(function() {
                            window.ParsleyUI.removeError( field, "asyncError");
                        }, 1000 );
                    }
                });
            }
        };
    }
})( angular );

angular.module('services', [
	'services.config',
	'services.utils',
	'services.title',
  'services.entity-manager',
  'services.modules-factory',
  'services.menu-manager',
  'services.metadata-manager'
]);

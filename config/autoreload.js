/**
 * Created by Андрей on 14.06.2015.
 */
module.exports.autoreload = {
  active: true,
  usePolling: false,
  dirs: [
    "api/models/SubjectAreas",
    "api/controllers/api/SubjectAreas"
  ]
};

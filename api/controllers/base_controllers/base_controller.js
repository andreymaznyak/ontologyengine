//var InterfaceConstructor = require('../../interfaceConstructor');
module.exports = function(ObjectName) {
  return {
    index: function (req, res, next) {
      global[ObjectName].find().exec(function (err, models) {
        if (err) next(err);

        res.view('forms/default_index',{
          models: models,
          objectname: ObjectName,
          widgetCollection: interfaceConstructor.getWidgetCollection(ObjectName, global[ObjectName]['_attributes'])
        });
      });
    },
    new: function (req, res) {
      res.view('forms/default_new',{objectname: ObjectName,
                widgetCollection: interfaceConstructor.getWidgetCollection(ObjectName, global[ObjectName]['_attributes'])});
    },

    create: function (req, res, next) {
      global[ObjectName].create(req.params.all(), function (err, model) {
        if (err) {
          req.session.flash = {
            err: err
          }
          console.log(err);
          return res.redirect('/Constructor/' + ObjectName + '/new', {message: err});
        }
        res.redirect('/Constructor/' + ObjectName + '/show/' + model.id);
      });
    },

    edit: function (req, res, next) {
      global[ObjectName].find().exec(function (err, models) {
        global[ObjectName].findOneById(req.param('id'), function (err, model) {
          if ( err || typeof(model) == 'undefined' ) return next(err);
          res.view('forms/default_edit',{
            objectname: ObjectName,
            model: model,
            title: 'Editing ' + model.title,
            models: models,
            widgetCollection: interfaceConstructor.getWidgetCollection(ObjectName, global[ObjectName]['_attributes'])
          });
        });
      });

    },

    update: function (req, res, next) {
      global[ObjectName].update({id: req.param('id')},
        req.params.all(),
        function (err, models) {
          if (err) {
            req.session.flash = {
              err: err
            }
            return res.redirect('/Constructor/' + ObjectName + '/edit/' + req.param('id'));
          }
          res.redirect('/Constructor/' + ObjectName + '/show/' + models[0].id);
        });
    },

    show: function (req, res, next) {
      global[ObjectName].findOneById(req.param('id'), function (err, model) {
        if (err) return next(err);
        if (!model) return next();
        res.view('forms/default_show',{
          objectname: ObjectName,
          model: model,
          title: model.title,
          widgetCollection: interfaceConstructor.getWidgetCollection(ObjectName, global[ObjectName]['_attributes'])
        });
      });
    },

    destroy: function (req, res, next) {
      global[ObjectName].findOneById(req.param('id'), function (err, model) {
        if (err) return next(err);
        if (model) model.destroy(function (err) {
          if (err)
            return next(err);
          else
            res.redirect('/Constructor/' + ObjectName);
        });
      })
    }
  }
}

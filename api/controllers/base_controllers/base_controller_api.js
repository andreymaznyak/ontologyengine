
var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');
var async = require('async');

actionUtil.parseModel = parseModel;
/**
 * Determine the model class to use w/ this blueprint action.
 * @param  {Request} req
 * @return {WLCollection}
 */
function parseModel(req) {

  // Ensure a model can be deduced from the request options.
  var model = req.options.model || req.options.controller;
  model = model.match(/.*\/([A-Za-zА-Я-а-я0-9_]+)\.?.*/);
  if( model ) model = model[1]
  if (!model) throw new Error(util.format('No "model" specified in route options.'));
  var Model = req._sails.models[model];


  if ( !Model ) throw new Error(util.format('Invalid route option, "model".\nI don\'t know about any models named: `%s`',model));

  return Model;
}
/**
 * Add Record To Collection
 *
 * post  /:modelIdentity/:id/:collectionAttr/:childid
 *  *    /:modelIdentity/:id/:collectionAttr/add/:childid
 *
 * Associate one record with the collection attribute of another.
 * e.g. add a Horse named "Jimmy" to a Farm's "animals".
 * If the record being added has a primary key value already, it will
 * just be linked.  If it doesn't, a new record will be created, then
 * linked appropriately.  In either case, the association is bidirectional.
 *
 * @param {Integer|String} parentid  - the unique id of the parent record
 * @param {Integer|String} id    [optional]
 *        - the unique id of the child record to add
 *        Alternatively, an object WITHOUT a primary key may be POSTed
 *        to this endpoint to create a new child record, then associate
 *        it with the parent.
 *
 * @option {String} model  - the identity of the model
 * @option {String} alias  - the name of the association attribute (aka "alias")
 */
function addToCollection(req, res) {

  // Ensure a model and alias can be deduced from the request.
  var Model = actionUtil.parseModel(req);

  var relation = req.param('alias');
  if (!relation) {
    return res.serverError(new Error('Missing required route option, `req.options.alias`.'));
  }

  // The primary key of the parent record
  var parentPk = req.param('parentid');

  // Get the model class of the child in order to figure out the name of
  // the primary key attribute.
  var associationAttr = _.findWhere(Model.associations, { alias: relation });
  var ChildModel = sails.models[associationAttr.collection];
  var childPkAttr = ChildModel.primaryKey;


  // The child record to associate is defined by either...
  var child;

  // ...a primary key:
  var supposedChildPk = actionUtil.parsePk(req);
  if (supposedChildPk) {
    child = {};
    child[childPkAttr] = supposedChildPk;
  }
  // ...or an object of values:
  else {
    req.options.values = req.options.values || {};
    req.options.values.blacklist = req.options.values.blacklist || ['limit', 'skip', 'sort', 'id', 'parentid'];
    child = actionUtil.parseValues(req);
  }

  if (!child) {
    res.badRequest('You must specify the record to add (either the primary key of an existing record to link, or a new object without a primary key which will be used to create a record then link it.)');
  }


  var createdChild = false;

  async.auto({

      // Look up the parent record
      parent: function (cb) {
        Model.findOne(parentPk).exec(function foundParent(err, parentRecord) {
          if (err) return cb(err);
          if (!parentRecord) return cb({status: 404});
          if (!parentRecord[relation]) return cb({status: 404});
          cb(null, parentRecord);
        });
      },

      // If a primary key was specified in the `child` object we parsed
      // from the request, look it up to make sure it exists.  Send back its primary key value.
      // This is here because, although you can do this with `.save()`, you can't actually
      // get ahold of the created child record data, unless you create it first.
      actualChildPkValue: ['parent', function(cb) {

        // Below, we use the primary key attribute to pull out the primary key value
        // (which might not have existed until now, if the .add() resulted in a `create()`)

        // If the primary key was specified for the child record, we should try to find
        // it before we create it.
        if (child[childPkAttr]) {
          ChildModel.findOne(child[childPkAttr]).exec(function foundChild(err, childRecord) {
            if (err) return cb(err);
            // Didn't find it?  Then try creating it.
            if (!childRecord) {return createChild();}
            // Otherwise use the one we found.
            return cb(null, childRecord[childPkAttr]);
          });
        }
        // Otherwise, it must be referring to a new thing, so create it.
        else {
          return createChild();
        }

        // Create a new instance and send out any required pubsub messages.
        function createChild() {
          ChildModel.create(child).exec(function createdNewChild (err, newChildRecord){
            if (err) return cb(err);
            if (req._sails.hooks.pubsub) {
              if (req.isSocket) {
                ChildModel.subscribe(req, newChildRecord);
                ChildModel.introduce(newChildRecord);
              }
              ChildModel.publishCreate(newChildRecord, !req.options.mirror && req);
            }

            createdChild = true;
            return cb(null, newChildRecord[childPkAttr]);
          });
        }

      }],

      // Add the child record to the parent's collection
      add: ['parent', 'actualChildPkValue', function(cb, async_data) {
        try {
          // `collection` is the parent record's collection we
          // want to add the child to.
          var collection = async_data.parent[relation];
          collection.add(async_data.actualChildPkValue);
          return cb();
        }
          // Ignore `insert` errors
        catch (err) {
          // if (err && err.type !== 'insert') {
          if (err) {
            return cb(err);
          }
          // else if (err) {
          //   // if we made it here, then this child record is already
          //   // associated with the collection.  But we do nothing:
          //   // `add` is idempotent.
          // }

          return cb();
        }
      }]
    },

    // Save the parent record
    function readyToSave (err, async_data) {

      if (err) return res.negotiate(err);
      async_data.parent.save(function saved(err) {

        // Ignore `insert` errors for duplicate adds
        // (but keep in mind, we should not publishAdd if this is the case...)
        var isDuplicateInsertError = (err && typeof err === 'object' && err.length && err[0] && err[0].type === 'insert');
        if (err && !isDuplicateInsertError) return res.negotiate(err);

        // Only broadcast an update if this isn't a duplicate `add`
        // (otherwise connected clients will see duplicates)
        if (!isDuplicateInsertError && req._sails.hooks.pubsub) {

          // Subscribe to the model you're adding to, if this was a socket request
          if (req.isSocket) { Model.subscribe(req, async_data.parent); }
          // Publish to subscribed sockets
          Model.publishAdd(async_data.parent[Model.primaryKey], relation, async_data.actualChildPkValue, !req.options.mirror && req, {noReverse: createdChild});
        }

        // Finally, look up the parent record again and populate the relevant collection.
        // TODO: populateEach
        Model.findOne(parentPk).populate(relation).exec(function(err, matchingRecord) {
          if (err) return res.serverError(err);
          if (!matchingRecord) return res.serverError();
          if (!matchingRecord[relation]) return res.serverError();
          return res.ok(matchingRecord);
        });
      });

    }); // </async.auto>
}


module.exports = {

  addToCollection: addToCollection,


/**
   * Find Records
   *
   *  get   /folders/:modelIdentity
   *   *    /folders/:modelIdentity/find
   *
   * An API call to find and return model instances from the data adapter
   * using the specified criteria.  If an id was specified, just the instance
   * with that unique id will be returned.
   *
   * Optional:
   * @param {Object} where       - the find criteria (passed directly to the ORM)
   * @param {Integer} limit      - the maximum number of records to send back (useful for pagination)
   * @param {Integer} skip       - the number of records to skip (useful for pagination)
   * @param {String} sort        - the order of returned records, e.g. `name ASC` or `age DESC`
   * @param {String} callback - default jsonp callback param (i.e. the name of the js function returned)
   */
  find: function (req, res) {
    // Look up the model
    var Model = actionUtil.parseModel(req);


    // If an `id` param was specified, use the findOne blueprint action
    // to grab the particular instance with its primary key === the value
    // of the `id` param.   (mainly here for compatibility for 0.9, where
    // there was no separate `findOne` action)
    if ( actionUtil.parsePk(req) ) {
      return getOne(req,res);
    }

    // Lookup for records that match the specified criteria
    var query = Model.find()
      .where( actionUtil.parseCriteria(req) )
      .limit( actionUtil.parseLimit(req) )
      .skip( actionUtil.parseSkip(req) )
      .sort( actionUtil.parseSort(req) );
    // TODO: .populateEach(req.options);
    query = actionUtil.populateEach(query, req);
    query.exec(function found(err, matchingRecords) {
      if (err) return res.serverError(err);

      // Only `.watch()` for new instances of the model if
      // `autoWatch` is enabled.
      if (req._sails.hooks.pubsub && req.isSocket) {
        Model.subscribe(req, matchingRecords);
        if (req.options.autoWatch) { Model.watch(req); }
        // Also subscribe to instances of all associated models
        _.each(matchingRecords, function (record) {
          actionUtil.subscribeDeep(req, record);
        });
      }

      res.ok(matchingRecords);
    });
  },
  getPage: function (req, res) {
    var page = req.param('page'),
        itemPerPage = req.param('itemcount'),
        Model = actionUtil.parseModel(req);
    if(!!page) page = 1;
    if(!!itemPerPage) itemPerPage = 30;
    Model
      .find()
      .sort({createdAt: 'desc'})
      .paginate({page: page, limit: itemPerPage})
      .then(function (models) {
        return [models];
      })
      .spread(function(models) {
        Model.watch(req);
        Model.subscribe(req, models);

        res.json(models);
      })
      .fail(function(err) {
        res.send(403);
      });
  },
  count: function (req, res) {
    var Model = actionUtil.parseModel(req);
    Model
      .count()
      .then(function (models) {
        return [models];
      })
      .spread(function(models) {
        var resultObject = { count: models };
        if(!models)
          models = 0
        res.json(resultObject);
      })
      .fail(function(err) {
        res.json({ count: 0 });
      });
  },
  /**
   * Find One Record
   *
   * get /:modelIdentity/:id
   *
   * An API call to find and return a single model instance from the data adapter
   * using the specified id.
   *
   * Required:
   * @param {Integer|String} id  - the unique id of the particular instance you'd like to look up *
   *
   * Optional:
   * @param {String} callback - default jsonp callback param (i.e. the name of the js function returned)
   */
  getOne: function (req, res) {
    var Model = actionUtil.parseModel(req);
    var pk = actionUtil.requirePk(req);

    var query = Model.findOne(pk).populateAll();
    //query = actionUtil.populateEach(query, req);
    query.exec(function found(err, matchingRecord) {
      if (err) return res.serverError(err);
      if(!matchingRecord) return res.notFound('No record found with the specified `id`.');

      if (sails.hooks.pubsub && req.isSocket) {
        Model.subscribe(req, matchingRecord);
        actionUtil.subscribeDeep(req, matchingRecord);
      }

      res.ok(matchingRecord);
    });
  },
  create: function (req,res){
    var Model = actionUtil.parseModel(req);

    // Create data object (monolithic combination of all parameters)
    // Omit the blacklisted params (like JSONP callback param, etc.)
    var data = actionUtil.parseValues(req);
    sails.log.debug(data);
    var associations = Model.associations;

    // Create new instance of model using data from params
    Model.create(data).exec(function created (err, newInstance) {

      // Differentiate between waterline-originated validation errors
      // and serious underlying issues. Respond with badRequest if a
      // validation error is encountered, w/ validation info.
      if (err) return res.negotiate(err);

      // If we have the pubsub hook, use the model class's publish method
      // to notify all subscribers about the created item
      if (req._sails.hooks.pubsub) {
        if (req.isSocket) {
          Model.subscribe(req, newInstance);
          Model.introduce(newInstance);
        }
        Model.publishCreate(newInstance, !req.options.mirror && req);
      }

      // Send JSONP-friendly response if it's supported
      res.created(newInstance);
    });
  },
  update: function updateOneRecord (req, res) {

    // Look up the model
    var Model = actionUtil.parseModel(req);

    // Locate and validate the required `id` parameter.
    var pk = actionUtil.requirePk(req);

    // Create `values` object (monolithic combination of all parameters)
    // But omit the blacklisted params (like JSONP callback param, etc.)
    var values = actionUtil.parseValues(req);

    // Omit the path parameter `id` from values, unless it was explicitly defined
    // elsewhere (body/query):
    var idParamExplicitlyIncluded = ((req.body && req.body.id) || req.query.id);
    if (!idParamExplicitlyIncluded) delete values.id;


    // Find and update the targeted record.
    //
    // (Note: this could be achieved in a single query, but a separate `findOne`
    //  is used first to provide a better experience for front-end developers
    //  integrating with the blueprint API.)
    Model.findOne(pk).populateAll().exec(function found(err, matchingRecord) {

      if (err) return res.serverError(err);
      if (!matchingRecord) return res.notFound();

      Model.update(pk, values).exec(function updated(err, records) {

        // Differentiate between waterline-originated validation errors
        // and serious underlying issues. Respond with badRequest if a
        // validation error is encountered, w/ validation info.
        if (err) return res.negotiate(err);


        // Because this should only update a single record and update
        // returns an array, just use the first item.  If more than one
        // record was returned, something is amiss.
        if (!records || !records.length || records.length > 1) {
          req._sails.log.warn(
            util.format('Unexpected output from `%s.update`.', Model.globalId)
          );
        }

        var updatedRecord = records[0];

        // If we have the pubsub hook, use the Model's publish method
        // to notify all subscribers about the update.
        if (req._sails.hooks.pubsub) {
          if (req.isSocket) { Model.subscribe(req, records); }
          Model.publishUpdate(pk, _.cloneDeep(values), !req.options.mirror && req, {
            previous: matchingRecord.toJSON()
          });
        }

        // Do a final query to populate the associations of the record.
        //
        // (Note: again, this extra query could be eliminated, but it is
        //  included by default to provide a better interface for integrating
        //  front-end developers.)
        var Q = Model.findOne(updatedRecord[Model.primaryKey]);
        Q = actionUtil.populateEach(Q, req);
        Q.exec(function foundAgain(err, populatedRecord) {
          if (err) return res.serverError(err);
          if (!populatedRecord) return res.serverError('Could not find record after updating!');
          res.ok(populatedRecord);
        }); // </foundAgain>
      });// </updated>
    }); // </found>
  },
  destroy: function destroyOneRecord (req, res) {

    var Model = actionUtil.parseModel(req);
    var pk = actionUtil.requirePk(req);

    var query = Model.findOne(pk);
    query = actionUtil.populateEach(query, req);
    query.exec(function foundRecord (err, record) {
      if (err) return res.serverError(err);
      if(!record) return res.notFound('No record found with the specified `id`.');

      Model.destroy(pk).exec(function destroyedRecord (err) {
        if (err) return res.negotiate(err);

        if (sails.hooks.pubsub) {
          Model.publishDestroy(pk, !sails.config.blueprints.mirror && req, {previous: record});
          if (req.isSocket) {
            Model.unsubscribe(req, record);
            Model.retire(record);
          }
        }

        return res.ok(record);
      });
    });
  }
}


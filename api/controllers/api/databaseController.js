/**
 * Created by Andrey Maznyak on 19.06.2015.
 * Module CreateDatabaseController
 */
var actionUtil = require('sails/lib/hooks/blueprints/actionUtil');

module.exports = {
  create: function(req,res){
    var pk = actionUtil.requirePk(req);
    var ModelOntology = req._sails.models.ontology;
    var query = ModelOntology.find({ where: { subjectarea: pk } }).populateAll();
    //Находим все онтологии
    query.exec(function found(err, matchingRecord) {
      if (err) return res.serverError(err);
      if(!matchingRecord || matchingRecord.length == 0) return res.notFound('No record found with the specified `id`.');
      //Находим все атрибуты связанные с объектами
      var ModelOntology = req._sails.models.attributesonto;
      ModelAttribute = req._sails.models.attributeonto;
      //Заполняем пути для генерации файлов
      paths = { models: this.sails.config.paths.models, controllers: this.sails.config.paths.controllers + '\\api' };//Множество путей где будут сгенерированны папки

      var subjectAreaPath = '\\SubjectAreas\\' + matchingRecord[0].subjectarea.name;
      for(var i in matchingRecord){
        var ontologyPath = subjectAreaPath + '\\' + matchingRecord[i].name
        matchingRecord[i].path = ontologyPath;
        for(var j in matchingRecord[i].objects){
          matchingRecord[i].objects[j].path = ontologyPath + '\\' + matchingRecord[i].objects[j].name;
        }
      }

      var subjectareaname = matchingRecord[0].subjectarea.name;
      async.map(matchingRecord, databaseManager.rebildOntologies, function(err, ontologies){

        if(err) throw err;

        var subjectArea = {
          name: subjectareaname,
          path: subjectAreaPath,
          ontologies: ontologies
        }

        databaseManager.writeModel(subjectArea, paths, function(err){ // this.sails.config.paths
          if (err) return res.serverError(err);
          res.ok(subjectArea);
        });
      });
    });
  }
}

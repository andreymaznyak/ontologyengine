/**
 * Created by Андрей on 01.06.2015.
 */

module.exports = {
  getAllFolders : function( req, res ) {
    metadataManager.getAllFolders( function( err, folders ){
      utils.sendRes( err, res, folders );
    })
  },
  getAllEntityInFolder : function( req, res ) {
    if(!req.param('folder'))
      res.json([]);
    else
      metadataManager.getAllEntityInFolder( req.param('folder'), function( err, folderAndEntities ){
        utils.sendRes( err, res, folderAndEntities );
      })
  }
};

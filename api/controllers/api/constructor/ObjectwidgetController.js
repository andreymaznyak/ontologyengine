/**
 * ObjectwidgetController
 *
 * @description :: Server-side logic for managing constructormodels
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
module.exports = _.merge(_.cloneDeep(require('../../base_controllers/base_controller_api')),{

  test: function(req,res){
    res.end('OK');
  }

});

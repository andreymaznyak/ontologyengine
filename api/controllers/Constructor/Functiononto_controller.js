/**
 * FunctionController
 *
 * @description :: Server-side logic for managing Functions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var fileNamePath = module.filename;
var ObjectName = fileNamePath.slice(fileNamePath.lastIndexOf("\\")+1,fileNamePath.lastIndexOf("Controller.js"));
var BaseController = require('../base_controllers/base_controller')(ObjectName);
module.exports =
{
  index : function(req,res,next) {
    BaseController.index(req,res,next);
  },
  new : function(req, res) {
    BaseController.new(req,res);
  },

  create : function (req, res, next) {
    BaseController.create(req, res, next);
  },

  edit : function(req, res, next) {
    BaseController.edit(req, res, next);
  },

  update : function (req, res, next) {
    BaseController.update(req, res, next);
  },

  show : function(req,res,next) {
    BaseController.show(req, res, next);
  },

  destroy : function(req,res,next) {
    BaseController.destroy(req, res, next);
  }
};


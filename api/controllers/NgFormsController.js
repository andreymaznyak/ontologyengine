/**
 * Created by Андрей on 14.06.2015.
 */
var _ = require('lodash');

function getURI(folder, name){
  return translit(folder + '/' + name, 1).replace('\\','/')
}
function getSref(folder, name){
  return translit(/*'sailng.' +*/'/' + folder + '/' + name, 1).replace('\\', '/')
}
//var InterfaceConstructor = interfaceConstructor;//require('../interfaceConstructor');
module.exports = {
  index: function (req, res) {
    var formname = req.param('formname'),
        formtype = req.param('formtype'),
        objectname = req.param('object');

    if ( !objectname ) {
      objectname = '';
    }

    objectname = _.capitalize(objectname.toLowerCase());

    var Model = global[objectname],
        key = '_attributes',
        Attributes;

    if(!Model && objectname != ''){
      Model = sails.models[objectname.toLowerCase()];
      key = 'definition';
    }
    if(objectname != ''){
      Attributes = Model[key];
    }

    if ( !formname ) {
      formname = 'default';
    }

    if ( !formtype ) {
      return res.badRequest('No formtype provided.');
    }
    res.view('forms/ng-forms/' + formname + '/' + formtype,{
      objectname: objectname,
      widgetCollection: interfaceManager.getWidgetCollection(objectname, Attributes)
    });
  },
  getsidebar: function ( req, res ){
    var formname = req.param('formname');
    if ( !formname ) formname = 'default';

    metadataManager.getAllFolders( function( err, folders ){
      var navItemsFolder = [];
      async.each( folders, function folderIterator( folder, callback ){
        metadataManager.getAllEntityInFolder( folder, function(err, folderAndEntities){
          var entities = folderAndEntities.entities,
              navItemsChild = [];
          for(var j in entities){
            navItemsChild.push( { title:entities[j],
                                  translationKey:'navigation:'+ entities[j],
                                  href: getURI(folder, entities[j]),
                                  cssClass:'fa fa-cog fa-spin',
                                  sref: getSref(folder, entities[j])
                                } );
          }
          var subTree = utils.parseUrlAndAddToTree( folderAndEntities.folder, navItemsChild, navItemsFolder );
          if( subTree )
            navItemsFolder = navItemsFolder.concat(subTree);
          callback(null);
        });
      }, function done( err ){
          if(err)
            res.send(500, err);
          else
            res.view('forms/ng-forms/' + formname + '/sidebar', {
              navItemsFolder: navItemsFolder
            });
      });
    });
  },
  getnavbar: function ( req, res ){
    var formname = req.param('formname');
    if ( !formname ) formname = 'default';

    metadataManager.getAllFolders( function( err, folders ){
      var navItemsFolder = [];
      async.each( folders, function folderIterator( folder, callback ){
        metadataManager.getAllEntityInFolder( folder, function(err, folderAndEntities){
          var entities = folderAndEntities.entities,
            navItemsChild = [];
          for(var j in entities){
            navItemsChild.push( { title:entities[j],
              translationKey:'navigation:'+entities[j],
              href: getURI(folder, entities[j]),
              cssClass:'fa fa-cog fa-spin'
            } );
          }
          var subTree = utils.parseUrlAndAddToTree( folderAndEntities.folder, navItemsChild, navItemsFolder );
          if( subTree )
            navItemsFolder = navItemsFolder.concat(subTree);
          callback(null);
        });
      }, function done( err ){
        if(err)
          res.send(500, err);
        else
          res.view('forms/ng-forms/' + formname + '/navbar', {
            navItemsFolder: navItemsFolder
          });
      });
    });
  },
  getWidgets: function getWidgets( req, res ){
    var entityName = req.param('entityname');
    var result = {
                  "structure": "main",
                  "title": entityName,
                  "rows": [
                    {
                      "columns": [
                        {
                          "styleClass": "col-md-3",
                          "widgets": [
                            {
                              "type": "Sidebar",
                              "config": {},
                              "title": "Sidebar"
                            }
                          ],
                          "cid": 1
                        },
                        {
                          "styleClass": "col-md-9",
                          "widgets": [],
                          "cid": 2
                        }
                      ]
                    }
                  ]
                },
        listWidget = {
          "type": 'list' + entityName,
          "config": {},
          "title": 'list ' + entityName
        },
        editWidet = {
          "type": 'edit' + entityName,
          "config": {},
          "title": 'edit ' + entityName
        },
        newWidget = {
          "type": 'new' + entityName,
          "config": {},
          "title": 'new ' + entityName
        };

    if(req.param('action') == 'list'){
      result.rows[0].columns[1].widgets.push(listWidget);
    }
    if(req.param('action') == 'edit'){
      result.rows[0].columns[1].widgets.push(editWidet);
    }
    if(req.param('action') == 'new'){
      result.rows[0].columns[1].widgets.push(newWidget);
    }
    console.log(req.param('uri'));
    res.json(result);
  }
}


/**
 * TypeWidget.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
    name:{
      type: 'String',
      required: true,
      regex:/^[a-zA-Z0-9]+$/
    },
    title:{
      type: 'String'
    },
    algoritm:{
      type: 'String'
    }
  }
};

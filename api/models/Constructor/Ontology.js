/**
 * Created by Andrey Maznyak on 16.06.2015.
 * Module Ontology
 */

var async = require('async');

function updateUri(values, cb){
  if(!values.id)
    cb();
  else
  Ontology.findOneById( values.id, function( err, result ){
    var tasks = null;
    if (err)
      cb(err);
    else
      if ( result.uri != values.uri && !!values.uri && values.updateUri ){
        tasks = [updateObjectOntos];
      }else if( !values.subjectarea || !values.name ||  (!!values.uri && ( result.name == values.name ))){
        var err = "Subject area was't defined";
        cb();
      }else{
        tasks = [updateOntology, updateObjectOntos];
      }
      if(!!tasks){
        async.series(tasks,
          function finaly(err){
            cb(err);
          }
        );
      }
  });

  function updateOntology(seriesCollback){
    Subjectarea.findOneById( values.subjectarea, function( err, result ){

      if( err ) return seriesCollback( err );

      if( !result ){
        err = "subject area not found";
        return seriesCollback( err );
      }
      values.uri = uriService.generateUri( values.name, result.uri );

      if( values.uri == null ){
        var err = "does't generate uri";
        return seriesCollback( err );
      }else{
        seriesCollback();
      }

    });
  }

  function updateObjectOntos(seriesCollback){
    Objectonto.find().where( { ontology: values.id } ).exec( function ( err, results ){
      async.each(results,function iterator( item, iteratorCollback ){
          Objectonto.update(item.id,{ id: item.id,  uri: uriService.generateUri( item.name, values.uri ), ontology: item.ontology }).exec(function(err){
            iteratorCollback(err);
          })
        },
        function(err){
          seriesCollback(err);
        })
    });
  }
}

module.exports = _.merge(_.cloneDeep(require('../base_models/base_metadata')),{
  attributes: {

    subjectarea: {
      model: 'Subjectarea'
    },

    objects: {
      collection: 'Objectonto',
      via: 'ontology'
    }
  },
  beforeCreate: function ( values, cb ) {
    updateUri( values, cb );
  },
  beforeUpdate: function ( values, cb ) {
    updateUri( values, cb );
  },
  uriOntology: function ( parentUri ){
    this.uri = 'hhh';
    return '1ff';
  }
});

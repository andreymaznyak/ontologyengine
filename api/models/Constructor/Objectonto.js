/**
* Objectonto.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
function updateUri(values, cb){
  if(!values.id)
    cb();
  else
    Objectonto.findOneById(values.id, function( err,result ){
      if(err)
        cb(err);
      else
        if(!values.ontology || !values.name || !!values.uri ||  (!!values.uri && ( result.name == values.name ))){
          var err = "ontology was't defined";
          cb();
        }else{
          Ontology.findOneById(values.ontology, function (err, result) {

            if (err) return cb(err);

            if (!result) {
              err = "ontology not found";
              return cb(err);
            }

            values.uri = uriService.generateUri(values.name, result.uri);

            if (values.uri == null) {
              var err = "does't generate uri";
              return cb(err);
            } else {
              cb();
            }

          });
        }
    });
}

module.exports = _.merge(_.cloneDeep(require('../base_models/base_metadata')),{

  attributes: {
    otherMethods: {
      collection: 'Functiononto',
      via: 'object'
    },
    attributes: {
      collection: 'Attributeonto',
      via: 'object'
    },
    ontology: {
      model: 'Ontology'
    },
    associateasobject:{
      collection: 'Associationonto',
      via: 'assciateby'
    }
  },
  beforeCreate: function (values, cb) {
    updateUri(values, cb);
  },
  beforeUpdate: function ( values, cb ) {
    updateUri( values, cb );
  }
});


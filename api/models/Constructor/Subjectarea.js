/**
 * Created by Andrey Maznyak on 16.06.2015.
 * Module SubjectArea
 */

var async = require('async');

function updateUri(values, cb) {
  async.series([
    function updateSubjectAreaUri(updateCollback){
      Subjectarea.findOneById(values.id, function ( err, result ) {
        if( !values.name ||  (!!values.uri && ( result.name == values.name ))){
          updateCollback();
        }else{
          values.uri = uriService.generateUri(values.name,"");

          if( values.uri == null ){
            var err = "does't generate uri";
            return updateCollback( err );
          }else{
            updateCollback();
          }
        }
      });
    },
    function updateOntologyUri(updateCollback){
      Ontology.find().where( { subjectarea: values.id } ).exec(function(err, results){
        async.each(results, function iterator(item, iteratorCollback) {
          Ontology.update(item.id,{ id: item.id, uri: uriService.generateUri( item.name, values.uri ), subjectarea: item.subjectarea, name: item.name, updateUri:true },
          function(err, result){
            if(err) iteratorCollback(err)
            else
            iteratorCollback()
          });
        },
        function ready(err){
          if(err)
          updateCollback(err)
          else
          updateCollback()
        });
      });
    }
  ],function finish(err){
    if (err) cb(err);
    else
    cb();
  })

}

module.exports = _.merge(_.cloneDeep(require('../base_models/base_metadata')),{

  attributes: {

    ontologies:{
      collection: 'Ontology',
      via: 'subjectarea'
    },

    geturi: function(){
      return uriService.generateUri(this.name,"");
    },
    createDB: {
      type:'boolean',
      defaultsTo:'false'
    }
    /*,
     include:{
     collection: 'Subjectarea',
     via: 'extend'
     },
     extend:{
     collection: 'Subjectarea',
     via: 'include'
     }*/
  },
  beforeCreate: function (values, cb) {
    updateUri(values, cb);
    if(values.createDB === true){

    }
  },
  beforeUpdate: function ( values, cb ) {
    updateUri( values, cb );
    if(values.createDB === true){

    }
  },
  uriS: function(){
    this.uri = 'YYY';
    return '1234dd';
  }

});

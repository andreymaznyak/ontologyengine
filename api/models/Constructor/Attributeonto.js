/**
* Attribute.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  //connection: 'somePostgresqlServer',//'localDiskDb',//,//,//////

  attributes: {
    type:{
      type: 'String',
      enum: ['string',
        'text',
        'integer',
        'float',
        'date',
        'datetime',
        'boolean',
        'binary',
        'array',
        'json',
        'email']
    },
    name:{
      type: 'String',
      required: true,
      regex:/^[a-zA-Z0-9]+$/
    },
    title:{
      type: 'String'
    },
    object:{
      model:'Objectonto'
    }
  }
};


/**
 * Associationsonto.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
    name: {
      type: 'String',
      regex:/^[a-zA-Z0-9]+$/
    },
    vianame: {
      type: 'String'
    },
    type:{
      type: 'String',
      enum: ['one to one',
            'one to many',
            'many to one',
            'many to many']
    },
    assciateby: {
      model: 'Objectonto'
    },
    associateto: {
      model: 'Objectonto'
    }
  }
};


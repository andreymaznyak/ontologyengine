/**
 * Created by Администратор on 03.07.2015.
 */

module.exports = {
  attributes:{
    name: {
      type:'String',
      required: true,
      regex:/^[a-zA-Z0-9]+$/
    },
    title:{
      type: 'String'
    },
    description:{
      type: 'String'
    },
    uri:{
      type:'String',
      size: 255,
      unique: true
    },
    setUri: function( uri ){
      this.uri = uri;
    }
  }
}

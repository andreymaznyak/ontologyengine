/**
 * Created by Андрей on 27.05.2015.
 */
var _ = require('lodash');
getDefaultWidget= function( name, attribute ) {
  var result;
  //sails.log.debug('&&' + name);
  //sails.log.debug(attribute);
  if ( ( attribute.type == 'string' || attribute.type == 'integer') && attribute.primaryKey != true && name != 'uri' ) {
    sails.log(name);
    if( attribute.enum )
      result = { name: 'inputSelect',
                 enum:attribute.enum }
    else
      result = { name:'inputText' }
  } else if ( attribute.type == 'boolean' ) {
    result = { name:'inputCheckbox' }
  } else if ( !!attribute && typeof( attribute.collection ) == 'string' ) {
    var objectName = _.capitalize(attribute.collection),
        tableRows = global[objectName]['definition'];
        widgets = getWidgetCollection( objectName, tableRows, attribute.via )
    result = { name:'collectionTable', widgets: widgets }
  } else if ( !!attribute && attribute.model  ) {
    result = { name:'inputChosen', model: 'constructor/' + attribute.model }
  }
  return result;
}
module.exports = {
  getWidgetCollection: getWidgetCollection
}

function getWidgetCollection(name, model, ignoringKey){
  var attributes = model;
  var widgets = {};
  //try{
  //  var assotiationObject = require('./models/' + name);
  //}
  //catch(e){
  //
  //}
  //sails.log.debug('Построение списка зависимостей виджетов для сущности ' + name + ' Виджеты:');
  var assotiationObject = {};
  for(var key in attributes){
    //sails.log(key + ' ? = ' + ignoringKey);
    if(key != ignoringKey) {
      var widget = assotiationObject[key];
      if(typeof (widget) == 'undefined' || widget == ''){
        widget = getDefaultWidget(key, attributes[key]);
      }
      if(typeof (widget) != 'undefined'){
        widgets[key] = widget;
      }
    }
  }
  //sails.log.debug(widgets);
  return widgets
}

var fs = require('fs'),
    async = require('async'),
    metadataManager = {};

function getEntityNamesInFolder(foldername, collback){
  var pathfolder = sails.config.paths.models + '\\' + foldername;
  var entities = [];

  fs.readdir( pathfolder, collback );
}

metadataManager.getAllFolders = function getAllFolders(callback){
  var err;

  var folders = ['Constructor'];
  var pathModels = sails.config.paths.models + '\\SubjectAreas';
  fs.readdir( pathModels, function(err, subjectareas){
    async.each(subjectareas, function iterator(subjectarea, callback){
      fs.readdir( pathModels + '\\' + subjectarea, function (err, ontologies) {
        for( var j in ontologies ) {
          folders.push('SubjectAreas\\' + subjectarea +'\\' + ontologies[j]);
        }
        callback(err);
      });
    }, function done(){
      callback( err, folders );
    });
  });
}

metadataManager.getAllEntityInFolder = function getAllEntityInFolder( folder, callback ){
  var err;
  getEntityNamesInFolder( folder, function( err, entities ){
    if( err )
      callback( err )
    else{
      var results = [];
      for(var i in entities){
        if (entities[i].lastIndexOf('.js') >= 0){
          results.push(entities[i].replace('.js',''));
        }
      }
      callback( err, { folder: folder, entities: results } );
    }
  });

}

module.exports = metadataManager;

/**
 * Created by AndreyMaznyak on 10.12.2015.
 */
var async = require('async'),
    fs = require('fs-extra'),
    dir_paths,
    errors = [];

function trowError(err){
  if(err) throw err;
  errors.push(err);
}

function compileSourceController(name){

  var result = "'use strict'; \n"
  result += " // module " + name + "Controller \n";
  result += "\n";
  result += "module.exports = _.merge(_.cloneDeep(require('../../../../base_controllers/base_controller_api')),{ \n" +
    "\n" +
    "});";

  return result;

}

function compileSourceModel( name, object ){

  var result = '';

  result += ' //Module ' + name + ' \n';
  result += '\n';
  result += 'module.exports = ';
  result += JSON.stringify(object, "", 4);

  return result;
}

function parseAssotiations(assotiations){
  var result = {};
  //sails.log.debug();
  for(var i in assotiations){
    if([i]){
      switch(assotiations[i].type){
        case 'one to one':
          result[assotiations[i].name] = { model: assotiations[i].associateto.name };
          break;
        case 'one to many':
          result[assotiations[i].name] = { model: assotiations[i].associateto.name };
          break;
        case 'many to one':
          result[assotiations[i].name] = { collection:assotiations[i].associateto.name, via: assotiations[i].vianame };
          break;
        case 'many to many':
          result[assotiations[i].name] = { collection: assotiations[i].associateto.name, via: assotiations[i].vianame };
          break;
      }
    }
  }
  return result;
}

function joinAttributes( object, callback ) {
  var query = Attributeonto.find({where: {object: object.id}});
  query.exec(function (err, results) {
    if(err) throw err;
    var attributes = {};
    for(var i in results){
      attributes[results[i].name] = {
        type: results[i].type
      };
    }
    var objectname = object.name.toLowerCase();
    var query = sails.models.associationonto.find({where: {assciateby: object.id}}).populateAll();
    query.exec(function (err, assotiations) {
      if(err) throw err;

      callback(err, {
          name: objectname,
          path: object.path,
          source: compileSourceModel( objectname ,{
            tableName: object.tableName,
            attributes: _.merge(attributes,parseAssotiations(assotiations)),
          })
        }
      );
    });


  });
}

function rebildOntologies( ontology, callback ) {

  async.map(ontology.objects, joinAttributes, function done(err, objects) {
    if(err) throw err;
    callback(err, { name: ontology.name, path: ontology.path, objects:objects } )
  });
}

function createDir( item, callback ){
  async.forEachOf(dir_paths, function(rootpath, key, callback){
    var path = rootpath + item.path;
    fs.exists(path, function (exists) {
      if(exists){
        //fs.remove(path, function(err){
        //if(err) throw err;
        //fs.mkdir(path, function(err){
        //  //if(err) throw err;
        callback(null);
        //callback(err)
        //})
        //});
      }
      else{
        fs.mkdir(path, function(err){
//          if(err) throw err;
          callback(err)
        })
      }

    });
  }, function done(err){
//    if(err) throw err;
    callback(err, item);
  });
}

function createOntologiesDirs( item, callback ){
  async.each(item.ontologies, createDir, function done(err){
    if(err) throw err;
    callback(err, item);
  })
}

function createObjects( subjectarea, callback ){

  async.each(subjectarea.ontologies, function ontologyIterator(ontology, callbackOntology){

    async.each(ontology.objects, function objectIterator(object, callbackObject){
      //Запись файла модели
      fs.writeFile(dir_paths.models + object.path + '.js', object.source, function(err) {
        if( err )
          callbackObject( err )
        else
        // Если запись модели прошла успешно записываем контроллер
          fs.writeFile(dir_paths.controllers + object.path + 'Controller.js', compileSourceController(object.name), function(err) {
            if(err) throw err;
            callbackObject(err);
          });
      });
    }, function doneObjects(err){
      if(err) throw err;
      callbackOntology(err);
    })

  }, function doneOntologies(err){
    if(err) throw err;
    callback(err);
  });

}

function writeModel(subjectarea, _paths, callback){
  dir_paths = _paths;
  async.waterfall([
      function(callback){
        callback( null, subjectarea )
      },
      createDir,
      createOntologiesDirs,
      createObjects
    ],
    function(err){
//      if(err) throw err;
      callback(err);
    });
}
function updateEntity(){

}
/*  */
function updateDirectory(path, delete_status){

      fs.exists(path, function (exists) {
        if(exists){
          //fs.remove(path, function(err){
          //if(err) throw err;
          //fs.mkdir(path, function(err){
          //  //if(err) throw err;
          if(!!delete_status)
            fs.remove(path, function(err) {
              callback(err);
            });
          else
            callback(null)

          //callback(err)
          //})
          //});
        }
        else{
          if(!delete_status)
          fs.mkdir(path, function(err){
//          if(err) throw err;
            callback(err)
          })
          else
            callback(null)
        }

      });
}


module.exports = {
  writeModel: writeModel,
  updateDirectory: updateDirectory,
  rebildOntologies: rebildOntologies
}

/**
 * Created by Администратор on 29.06.2015.
 */

var utils = {};

utils.sendRes = function sendRes( err, res, result ){
  if( err )
    res.send( 500, err )
  else
    res.json( result );
}

utils.parseUrlAndAddToTree = function parseUrlAndAddToTree(uri, lastChild, navItemsFolder){
  sails.log.debug( uri );
  var indexComma = uri.indexOf('\\'),
    lastComma = uri.lastIndexOf('\\'),
    currentName = 'error',
    resultSubTree = [],
    currentList = [],
    newSubTree = [],
    navItemsChild;
  if( indexComma > 0 ) {
    currentName = uri.substr(0,indexComma);
    var nextUri = uri.replace(currentName + '\\','');
    navItemsChild = [];

    newSubTree = utils.parseUrlAndAddToTree( nextUri, lastChild, navItemsFolder );
    if( newSubTree )
      navItemsChild = navItemsChild.concat( newSubTree );
    currentList = [{title:currentName, translationKey:'navigation:'+currentName, cssClass:'fa fa-cog', "submenu": navItemsChild }];

    resultSubTree = currentList.concat(resultSubTree);
  }
  else {
    currentName = uri;
    navItemsChild = lastChild;
    resultSubTree.push({title:currentName, translationKey:'navigation:'+currentName,/* cssClass:'fa fa-cog', */"submenu": navItemsChild });
    if( currentName == 'Constructor' ){
      resultSubTree[0]['ngif'] = 'CurrentUser';
    }
  }
  var result = _.findWhere(navItemsFolder,{ title:currentName })
  if( !result ){
    result =  _.findWhere(navItemsFolder,{ header:currentName })
    if( !result )
      return resultSubTree
    else
      return newSubTree
  }
  else
    result.submenu = result.submenu.concat( navItemsChild );
}

module.exports = utils;

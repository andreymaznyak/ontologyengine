describe('Objectonto', function() {
  it ('should not be empty', function(done) {
    Objectonto.find().exec(function(err, objectontos) {
      objectontos.length.should.be.eql(fixtures['objectonto'].length);
      done();
    });
  });
});

var request = require('supertest');
var fixtures = {
  objectonto: [
    {
      "name": "test1",
      "title": "test1",
      "tableName": "test1",
      "adapter": "test1",
      "schema": "test1",
      "autoCreatedAt": false,
      "autoPK": false,
      "id": 199,
      "createdAt": "2015-06-15T03:56:15.674Z",
      "updatedAt": "2015-06-15T03:56:15.674Z"
    }
  ]
}



describe('EntityManagerController', function() {
  describe('getAllFolders', function() {
    it('Должен вернуть список папок в директории api/models', function (done) {
      request(sails.hooks.http.app)
        .get('/api/entitymanager/getAllFolders')
        .expect(200, done);
    });
  });
  describe('getFolder', function() {
    it('Должен вернуть список папок в директории api/models', function (done) {
      request(sails.hooks.http.app)
        .get('/api/entitymanager/getAllFolder')
        .expect(404, done);
    });
    it('Вернуть все сущности в папке Constructor', function () {
      request(sails.hooks.http.app)
        .post('/api/entitymanager/getAllFolder')
        .send({'folder': 'Constructor'})
        .expect(200, []);
    });
  });
});

function get200( url , done ) {
  //request(sails.hooks.http.app)
  //  .get(url)
  //  .expect(200, done );
};

function getAll( done ) {
  it('Должно вернуть все объекты онтологии', get200('/api/constructor/objectonto/getAll' , done ));
};

describe('ObjecontoControllerApi', function() {

  describe('getAll', getAll );


    describe('destroy', function(done) {
      var i = 0;
      //for(i in fixtures['objectonto'])
      //{
        it('Должен удалить объект id =' + fixtures['objectonto'][i].id, function (done) {
          request(sails.hooks.http.app)
            .delete('/api/constructor/objectonto/destroy/'+ fixtures['objectonto'][i].id)
            .expect(200, done);
        });
     // }
      it('Данного объекта нет, должен вернуть 404 id =' + fixtures['objectonto'][i].id, function (done) {
        request(sails.hooks.http.app)
          .post('/api/constructor/objectonto/destroy/'+ fixtures['objectonto'][i].id)
          .send(fixtures['objectonto'][i])
          .expect(404, done);
      });
    });

    it('Должно вернуть пустой список, т.к. онтологии были удалены', function (done) {
      request(sails.hooks.http.app)
        .get('/api/constructor/objectonto/getAll')
        .expect([], done);
    });

    describe('create', function(done) {
      var i = 0;
      it('Должен создать объект id =' + fixtures['objectonto'][i].id, function (done) {
        request(sails.hooks.http.app)
          .post('/api/constructor/objectonto/create')
          .send(fixtures['objectonto'][i])
          .expect(200, done);
      });

      it('Данный объект уже есть, должен выругаться id =' + fixtures['objectonto'][i].id, function (done) {
        request(sails.hooks.http.app)
          .post('/api/constructor/objectonto/create')
          .send(fixtures['objectonto'][i])
          .expect(400, done);
      });
    });

    it('Должно вернуть все объекты онтологии, список должен равным тестовому набору', function (done) {
      request(sails.hooks.http.app)
        .get('/api/constructor/objectonto/getAll')
        .expect(fixtures['objectonto'][1], done);
    });

});






describe('ObjecontoController', function() {
  describe('getAll', function() {
    it('Должно вернуть все объекты онтологии', function (done) {
      request(sails.hooks.http.app)
        .get('/api/constructor/objectonto/getAll')
        .expect(200, done);
    });
  });
});


